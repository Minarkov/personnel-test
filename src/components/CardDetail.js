import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

const CardDetail = (props) => {
  const { id } = useParams();
  const [currentProduct, setCurrentProduct] = useState([]);

  // get request for dynamically adding id for card details

  useEffect(() => {
    axios
      .get(`https://world.openfoodfacts.org/api/v0/product/${id}.json`)
      .then((res) => {
        const product = res.data.product;
        setCurrentProduct(product);
      });
  }, [id]);

  return (
    <Fragment>
      <div className="cardDetail container-fluid">
        <div className="row">
          <div className="col-md-6 text-center card-col">
            <img src={currentProduct.image_url} alt="img" />
          </div>
          <div className="col-md-4 col-md-offset-2 info-cont">
            <h2 className="title">{currentProduct.product_name_fr}</h2>
            <p>{currentProduct.brands}</p>
            <h3>{currentProduct.countries}</h3>
            {currentProduct.allergens_from_ingredients === "" ? (
              <p>There are not any allergens</p>
            ) : (
              <p className="p-bold">
                Allergens: {currentProduct.allergens_from_ingredients}
              </p>
            )}
            <img src={currentProduct.image_ingredients_url} alt="ingredients" />
            {currentProduct.ingredients_text_fr === "" ? (
              <p>There are not any ingredients</p>
            ) : (
              <p className="p-bold">
                Ingredients:
                {currentProduct.ingredients_text_fr}
              </p>
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default CardDetail;
