import React from "react";

const Footer = () => {
  return (
    <div className="footer container-fluid">
      <div className="row lists">
        <div className="container">
          <div className="row">
            <div className="col-md-10 col-md-offset-1">
              <div className="row">
                <div className="col-md-4">
                  <h2>company</h2>
                  <ul>
                    <li>
                      <a href="/">about</a>
                    </li>
                    <li>
                      <a href="/">store location</a>
                    </li>
                    <li>
                      <a href="/">pizza styles</a>
                    </li>
                    <li>
                      <a href="/">pizza brands</a>
                    </li>
                  </ul>
                </div>
                <div className="col-md-4">
                  <h2>support</h2>
                  <ul>
                    <li>
                      <a href="/">contact</a>
                    </li>
                    <li>
                      <a href="/">terms</a>
                    </li>
                    <li>
                      <a href="/">privacy policy</a>
                    </li>
                  </ul>
                </div>
                <div className="col-md-4">
                  <h2>company</h2>
                  <ul>
                    <li>
                      <a href="/">about</a>
                    </li>
                    <li>
                      <a href="/">store location</a>
                    </li>
                    <li>
                      <a href="/">collection</a>
                    </li>
                    <li>
                      <a href="/">brands</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="row copyright">
        <div className="col-md-12">
          <p>
            Copyrights &copy; 2020 <span>Personnel Test</span>. All rights not
            reserved! =)
          </p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
