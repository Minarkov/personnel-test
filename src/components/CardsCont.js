import React, { useContext } from "react";
import Card from "./Card";
import Filters from "./Filters";
import PaginationComponent from "../components/PaginationComponent";
import { uploudingContext } from "../Context/TaskListContext";

function CardsCont({ allproducts, loading }) {
  const {
    products,
    productsPerPage,
    paginate,
    currentPage,
  } = useContext(uploudingContext);

  if (loading) {
    return <h2>Loading...</h2>;
  } else {
    return (
      <div className="cardCont container-fluid">
        <div className="row home-wrapper">
          <div className="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1  col-sm-10 col-sm-offset-1 col-xs-8 col-xs-offset-2">
            <div className="row">
              <h2 className="title">Best pizza in town!</h2>
            </div>
            <div className="row">
              <Filters />
            </div>
            <div className="row cardWrapper">
              {allproducts.map((el) => (
                <Card
                  key={el.id}
                  productname={el.product_name_fr}
                  brands={el.brands}
                  id={el.id}
                  img={el.image_url}
                  countries={el.countries}
                />
              ))}
            </div>
            <div className="row">
              <PaginationComponent
                allproducts={products}
                productsPerPage={productsPerPage}
                paginate={paginate}
                currentPage={currentPage}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CardsCont;
