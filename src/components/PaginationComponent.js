import React, { Fragment } from "react";
import Pagination from "react-js-pagination";

const PaginationComponent = ({
  productsPerPage,
  allproducts,
  paginate,
  currentPage,
}) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(allproducts / productsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <Fragment>
      <Pagination
        activePage={currentPage}
        totalItemsCount={allproducts.length + 10}
        pageRangeDisplayed={productsPerPage}
        onChange={paginate}
      />
    </Fragment>
  );
};

export default PaginationComponent;
