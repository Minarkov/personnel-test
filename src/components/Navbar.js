import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navbar">
      <nav className="nav menu">
        <div className="container">
          <div className="row">
            <div className="col-md-10 col-md-offset-1">
              <div className="navbar-header">
                <Link to="/" className="navbar-brand">
                  Pizza Fest
                  {/* <img
                    src=""
                    alt="Logo"
                  /> */}
                </Link>
              </div>
              <div className="collapse navbar-collapse" id="menu-button">
                <ul className="nav navbar-nav navbar-right">
                  <li>
                    <Link to="/">Home</Link>
                  </li>
                  <li>
                    <a href="/">Menu</a>
                  </li>
                  <li>
                    <a href="/">Recipes</a>
                  </li>
                  <li>
                    <a href="/">restaurants</a>
                  </li>
                  <li>
                    <a href="/">Contact</a>
                  </li>
                  <li>
                    <a href="/">About us</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
