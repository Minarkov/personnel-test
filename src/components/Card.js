import React from "react";
import { Link } from "react-router-dom";

// Single Card with props from CardsCont component

const Card = ({ id, productname, brands, img, countries }) => {
  return (
    <div className="col-md-4 col-sm-6 col-xs-12 h-100">
      <div className="card">
        <Link to={`/Details/${id}`}>
          {img === undefined ? (
            <img src="https://image.shutterstock.com/image-vector/no-image-available-icon-photo-260nw-1251146485.jpg" alt="" />
          ) : (
            <img src={img} alt="" />
          )}
          <div className="card-footer">
            <h2>{productname}</h2>
            <p>{brands}</p>
            <p>{countries}</p>
          </div>
        </Link>
      </div>
    </div>
  );
};

export default Card;
