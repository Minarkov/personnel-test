import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import CardsCont from "../components/CardsCont";
import CardDetail from "../components/CardDetail";
import ErrorPage from "../components/ErrorPage";
import ScrollToTop from "../components/ScrollToTop";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { uploudingContext } from "../Context/TaskListContext";
import axios from "axios";

function Wrapper(props) {
  const {
    products,
    productsPerPage,
    setAllproducts,
    currentPage,
    loading,
    setLoading,
  } = useContext(uploudingContext);

  useEffect(() => {
    axios
      .get(
        `https://world.openfoodfacts.org/cgi/search.pl?action=process&tagtype_0=categories&tag_contains_0=contains&tag_0=pizzas&json=true`
      )
      .then((res) => {
        const all = res.data.products;
        setAllproducts(all);
        setLoading(false);
      });
  }, [setAllproducts, setLoading]);

  // pagination

  const indexOfLastPost = currentPage * productsPerPage;
  const indexOfFirstPost = indexOfLastPost - productsPerPage;
  const currentProducts = products.slice(indexOfFirstPost, indexOfLastPost);

  return (
    <Router>
      <ScrollToTop>
        <div className="App">
          <Navbar />
          <Switch>
            <Route
              exact
              path="/"
              render={(props) => (
                <CardsCont
                  {...props}
                  allproducts={currentProducts}
                  loading={loading}
                />
              )}
            />
            <Route
              path="/Details/:id"
              render={(props) => <CardDetail {...props} />}
            />
            <Route component={ErrorPage} />
          </Switch>
          <Footer />
        </div>
      </ScrollToTop>
    </Router>
  );
}

export default Wrapper;
