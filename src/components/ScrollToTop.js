import { Component } from "react";
import { withRouter } from "react-router-dom";

class ScrollToTop extends Component {

  // It is not a good practise to use hooks and class component together! this is just example of class component in
  // React Js!

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }
  render() {
    return this.props.children;
  }
}

export default withRouter(ScrollToTop);
