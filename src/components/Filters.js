import React, { useState, useEffect, useContext } from "react";
import { uploudingContext } from "../Context/TaskListContext";
import axios from "axios";

function Filters(props) {
  const [addingNewIngredient, setNewingredient] = useState([]);
  const { setAllproducts, setLoading, setCurrentPage } = useContext(
    uploudingContext
  );

  useEffect(() => {
    let qstring = "";
    addingNewIngredient.forEach((el, i) => {
      qstring += `&tagtype_${i + 1}=ingredients&tag_contains_${
        i + 1
      }=contains&tag_${i + 1}=${el}`;
    });

    axios
      .get(
        encodeURI(
          `https://world.openfoodfacts.org/cgi/search.pl?action=process&tagtype_0=categories&tag_contains_0=contains&tag_0=pizzas${qstring}&json=true`
        )
      )
      .then((res) => {
        const byIngredients = res.data.products;
        setAllproducts(byIngredients);
        setLoading(false);
        setCurrentPage(1);
      });
  }, [addingNewIngredient, setAllproducts, setLoading, setCurrentPage]);
  const onChange = (e) => {
    if (e.target.checked) {
      setNewingredient([...addingNewIngredient, e.target.value]);
    } else {
      const filteredarray = addingNewIngredient.filter(
        (el) => el !== e.target.value
      );
      setNewingredient(filteredarray);
    }
  };
  return (
    <form>
      <fieldset>
        <legend>Ingredients:</legend>
        <div className="form-check">
          <label htmlFor="form-check-input">emmental</label>
          <input
            type="checkbox"
            className="form-check-input"
            onChange={onChange}
            value="emmental"
            name="emmental"
          />
        </div>
        <div className="form-check">
          <label htmlFor="form-check-input">gorgonzola</label>
          <input
            type="checkbox"
            className="form-check-input"
            onChange={onChange}
            value="gorgonzola"
            name="gorgonzola"
          />
        </div>
        <div className="form-check">
          <label htmlFor="form-check-input">parmesan</label>
          <input
            type="checkbox"
            className="form-check-input"
            onChange={onChange}
            value="parmesan"
            name="parmesan"
          />
        </div>
        <div className="form-check">
          <label htmlFor="form-check-input">edam</label>
          <input
            type="checkbox"
            className="form-check-input"
            onChange={onChange}
            value="edam"
            name="edam"
          />
        </div>
        <div className="form-check">
          <label htmlFor="form-check-input">mozzarela</label>
          <input
            type="checkbox"
            className="form-check-input"
            onChange={onChange}
            value="mozzarella"
            name="mozzarella"
          />
        </div>
        <div className="form-check">
          <label htmlFor="form-check-input">gouda</label>
          <input
            type="checkbox"
            className="form-check-input"
            onChange={onChange}
            value="gouda"
            name="gouda"
          />
        </div>
        <div className="form-check">
          <label htmlFor="form-check-input">ricotta</label>
          <input
            type="checkbox"
            className="form-check-input"
            onChange={onChange}
            value="ricotta"
            name="ricotta"
          />
        </div>
        <div className="form-check">
          <label htmlFor="form-check-input">champignons</label>
          <input
            type="checkbox"
            className="form-check-input"
            onChange={onChange}
            value="champignons"
            name="champignons"
          />
        </div>
        <div className="form-check">
          <label htmlFor="form-check-input">olives</label>
          <input
            type="checkbox"
            className="form-check-input"
            onChange={onChange}
            value="olives"
            name="olives"
          />
        </div>
      </fieldset>
    </form>
  );
}

export default Filters;
