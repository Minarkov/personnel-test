import React from "react";
import "./App.css";
import Wrapper from "./components/Wrapper";
import { TaskListContextProvider } from "./Context/TaskListContext";

const App = () => {
  return (
    <TaskListContextProvider>
      <Wrapper />
    </TaskListContextProvider>
  );
};

export default App;
