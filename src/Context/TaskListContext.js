import React, { createContext, useState } from "react";
export const Context = createContext({});

export const Provider = (props) => {
const [loading, setLoading] = useState(false);
const [products, setAllproducts] = useState([]);
const [productsPerPage] = useState(6);
const paginate = (pageNumber) => setCurrentPage(pageNumber);
const [currentPage, setCurrentPage] = useState(1);

  const uploudingContext = {
    products, setAllproducts, productsPerPage, paginate, currentPage,
    setCurrentPage, loading, setLoading
  };

  return (
    <Context.Provider value={uploudingContext}>
      {props.children}
    </Context.Provider>
  );
};

export const TaskListContextProvider = Provider;
export const uploudingContext = Context;
